package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";
	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String BRANCHFILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String COMMODITYFILE_NOT_EXIST = "商品定義ファイルが存在しません";
	private static final String BRANCHFILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String COMMODITYFILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";
	private static final String NOT_SERIAL_NUMBER = "売り上げファイル名が連番になっていません";
	private static final String OVEVR_BILION = "合計金額が10桁を超えました";
	private static final String BRANCHCODE_IS_INCORRECT = "の支店コードが不正です";
	private static final String COMMODITYCODE_IS_INCORRECT = "の商品コードが不正です";
	private static final String FORMAR_IS_INCORRECT = "のフォーマットが不正です";

	/**
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が正しくなされているか 3-1
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//1-4商品コードと、商品名を保持するMap
		Map<String, String> itemNames = new HashMap<>();
		//商品コードと、売上金額を保持するMap
		Map<String, Long> itemSales = new HashMap<>();

		//支店定義ファイルバリデーション用の正規表現
		String branchValid = "^[0-9]{3}";
		//商品定義ファイルバリデーション用の正規表現
		String itemValid = "^[0-9a-zA-Z]{8}";

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchValid, BRANCHFILE_NOT_EXIST, BRANCHFILE_INVALID_FORMAT, branchNames, branchSales)) {
			return;
		}
		//1-3商品定義ファイル読み込み
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, itemValid, COMMODITYFILE_NOT_EXIST, COMMODITYFILE_INVALID_FORMAT, itemNames, itemSales)) {
			return;
		}

		//2-1正規表現に当てはまる売り上げファイルを取得、リストに格納
		List<File> rcdFile = new ArrayList<>();
		File[] files = new File(args[0]).listFiles();
		/////売り上げファイルかどうかの判定
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().matches("^[0-9]{8}\\.rcd$") && files[i].isFile()) {
				rcdFile.add(files[i]);
			}
		}
		//連番の確認 バリデーション 2-1
		Collections.sort(rcdFile);
		for (int i = 0; i < rcdFile.size() - 1; i++) {
			int former = Integer.parseInt(rcdFile.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFile.get(i + 1).getName().substring(0, 8));
			if (latter - former != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		//2-2売り上げファイルの読み込み
		BufferedReader br = null;
		for (int i = 0; i < rcdFile.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFile.get(i));
				br = new BufferedReader(fr);
				List<String> fileContents = new ArrayList<>();
				String line = null;
				try {
					while ((line = br.readLine()) != null) {
						fileContents.add(line);
					}
					/////////売り上げげファイルが本当に3行かどうか バリデーション2-5
					if (fileContents.size() != 3) {
						System.out.println(rcdFile.get(i).getName() + FORMAR_IS_INCORRECT);
						return;
					}
					////？変数宣言をして、以降の文を簡略化させるか、直書きするか。
					String branch = fileContents.get(0);
					String itemCode = fileContents.get(1);
					String price = fileContents.get(2);

					//売り上げファイルの店番が本当に存在しているか バリデーション 2-3
					if (!branchSales.containsKey(branch)) {
						System.out.println(rcdFile.get(i).getName() + BRANCHCODE_IS_INCORRECT);
						return;
					}
					//商品コードが本当に存在しているかどうか バリデーション 2-4
					if (!itemSales.containsKey(itemCode)) {
						System.out.println(rcdFile.get(i).getName() + COMMODITYCODE_IS_INCORRECT);
						return;
					}

					//入力されている売上金額が数字かどうか 3-2
					if (!price.matches("^[0-9]+$")) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}

					Long saleAmount = Long.parseLong(price);
					Long saleAmountItem = itemSales.get(itemCode) + saleAmount;
					saleAmount += branchSales.get(branch);

					//支店または商品別の金額1 0桁超えているかどうか バリデーション 2-2
					if (saleAmount >= 1000000000L || saleAmountItem >= 1000000000L) {
						System.out.println(OVEVR_BILION);
						return;
					}

					branchSales.put(branch, saleAmount);
					itemSales.put(fileContents.get(1), saleAmountItem);

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
				}
			} catch (FileNotFoundException e) {
				System.out.println(UNKNOWN_ERROR);
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, itemNames, itemSales)) {
			return;
		}
	}

	/**
	 * ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String valid, String notFoundError,String formatError, Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			//ファイルの有無確認 バリデーション1-1 1-3
			if (!file.exists()) {
				System.out.println(notFoundError);
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			String items[];
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				items = line.split(",");
				//ファイルのフォーマット確認 バリデーション 1-2 1-4
				if (!items[0].matches(valid) || items.length != 2) {
					System.out.println(formatError);
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], (long) 0);
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		File file = new File(path, fileName);
		BufferedWriter bw = null;
		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String branchNum : sales.keySet()) {
				bw.write(branchNum + "," + names.get(branchNum) + "," + sales.get(branchNum));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
				}
			}
		}

		return true;
	}

}
